import MySQLdb as mysql
from MySQLdb.cursors import DictCursor
import sys
from _settings import *
if not len(sys.argv) == 4:
	print "Usage: %s 'keywords' minprice maxprice"%sys.argv[0]
	exit()
keywords = sys.argv[1]
try:
	minprice = float(sys.argv[2])
except:
	minprice = None
try:
	maxprice = float(sys.argv[3])
except:
	maxprice = None
con = mysql.connect(host=DBHOST, user=DBUSERNAME, passwd=DBPASSWORD,
		db=DBDATABASE, cursorclass=DictCursor)
cur = con.cursor()
sql = """select id,description,price,url,
		match(description) against('%s') as relevance
		from `"""%keywords.replace("'", "''") + DBTABLE + """`
		where match(description)
		against('%s')"""%keywords.replace("'", "''")
if minprice:
	sql += " and price>=%d"%minprice
if maxprice:
	sql += " and price<=%d"%maxprice
cur.execute(sql)
rows = cur.fetchall()
if rows:
	for row in rows:
		print row['id'], row['description'], row['price'], row['url'], row['relevance']
else:
	print "no results"

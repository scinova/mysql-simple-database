import MySQLdb as mysql
from MySQLdb.cursors import DictCursor
import sys
import pickle
from _settings import *
con = mysql.connect(host=DBHOST, user=DBUSERNAME, passwd=DBPASSWORD,
		db=DBDATABASE, cursorclass=DictCursor)
cur = con.cursor()
sql = """create table `%s` (
		id varchar(256) not null,
		description text not null,
		price float not null,
		url varchar(256) not null,
		primary key (id),
		key price (price),
		fulltext key description (description)
		) ENGINE=MyISAM"""%DBTABLE
cur.execute(sql)
data = pickle.load(file(DATAFILE))
for itemid, description, price, url in data:
	print itemid, description, price, url
	cur.execute("""insert into `""" + DBTABLE + """`
			(id, description, price, url)
			values(%s, %s, %s, %s)""", (itemid, description, price, url))
